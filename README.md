Конвертер PDF в HTML слайдер
===============================

Описание
-------------------

```
Приложение представляет собой конвертер PDF-файлов в HTML-слайдер.
На странице с созданным слайдером доступно действие «Скачать», при нажатии на которую скачивается zip архив, содержащий:
    index.html, который содержит слайдер;
    папку images, в которой хранятся изображения, конвертированные из PDF;
    папку assets, в которой хранятся необходимые ресурсы, например JS- библиотека слайдера.

Требования к загружаемому файлу:
    Количество страниц – не больше 20;
    Размер файла – не более 50 Мб.

Также в приложении реализован REST api метод для получения списка ссылок изображений конкретного слайдера (по его id) для генерации кастомных слайдеров на сторонних сайтах (json).
Пример обращения к методу:
    http://pdf-to-slider.ru/control-panel/api/slider/get-slider/{id}
```
