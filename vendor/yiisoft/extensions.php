<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  '2amigos/yii2-date-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-picker-widget',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@dosamigos/datepicker' => $vendorDir . '/2amigos/yii2-date-picker-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'iutbay/yii2-fontawesome' => 
  array (
    'name' => 'iutbay/yii2-fontawesome',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@iutbay/yii2fontawesome' => $vendorDir . '/iutbay/yii2-fontawesome',
    ),
  ),
  'PHPDOC' => 
  array (
    'name' => 'PHPDOC',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@PHPDOC' => $vendorDir . '/phpopendoc/PHPDOC',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker',
    ),
  ),
  'nkovacs/yii2-datetimepicker' => 
  array (
    'name' => 'nkovacs/yii2-datetimepicker',
    'version' => '3.0.3.0',
    'alias' => 
    array (
      '@nkovacs/datetimepicker' => $vendorDir . '/nkovacs/yii2-datetimepicker',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'linslin/yii2-curl' => 
  array (
    'name' => 'linslin/yii2-curl',
    'version' => '1.0.8.0',
    'alias' => 
    array (
      '@linslin/yii2/curl' => $vendorDir . '/linslin/yii2-curl',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  '2amigos/yii2-multi-select-widget' => 
  array (
    'name' => '2amigos/yii2-multi-select-widget',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@dosamigos/multiselect' => $vendorDir . '/2amigos/yii2-multi-select-widget',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert',
    ),
  ),
  'nickcv/yii2-encrypter' => 
  array (
    'name' => 'nickcv/yii2-encrypter',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@nickcv/encrypter' => $vendorDir . '/nickcv/yii2-encrypter',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.16.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.2.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'miloschuman/yii2-highcharts-widget' => 
  array (
    'name' => 'miloschuman/yii2-highcharts-widget',
    'version' => '5.0.0.0',
    'alias' => 
    array (
      '@miloschuman/highcharts' => $vendorDir . '/miloschuman/yii2-highcharts-widget/src',
    ),
  ),
  'kartik-v/yii2-dropdown-x' => 
  array (
    'name' => 'kartik-v/yii2-dropdown-x',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/dropdown' => $vendorDir . '/kartik-v/yii2-dropdown-x',
    ),
  ),
  'kartik-v/yii2-nav-x' => 
  array (
    'name' => 'kartik-v/yii2-nav-x',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/nav' => $vendorDir . '/kartik-v/yii2-nav-x',
    ),
  ),
);
