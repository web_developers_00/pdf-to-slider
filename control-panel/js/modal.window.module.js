(function() {

	//render modal window on link click
	$('.modalButton').click(function(e) {
        e.preventDefault(); //for prevent default behavior of <a> tag.
        var tagname = $(this)[0].tagName;
        $('#editModalId').modal('show').find('.modalContent').empty();
        $('#editModalId').modal('show').find('#time-progressbar').show();
        $('#editModalId').modal('show').find('.modalContent').load($(this).attr('href'), function() {
            $('#editModalId').modal('show').find('#time-progressbar').hide();
        });
    });

	//submit modal form
	$("body").on("beforeSubmit", "form#modal-form", function () {
       var form = $(this);
       // return false if form still have some validation errors
       if (form.find(".has-error").length) {
           return false;
       }
       // submit form
       $.ajax({
           url: form.attr("action"),
           type: "post",
           data: form.serialize(),
           success: function (response) {
           		console.log(response);
               if (response) {
                   form.find(".alert").show();
                   form.find(".alert").text(response);
                   return false;
               }
               $("#editModalId").modal("toggle");
               //$.pjax.reload({container:"#lotcontrol-grid-container-id"}); //for pjax update
           },
           error: function (response) {
           		/*form.find(".alert").show();
                form.find(".alert").text('Ошибка сервера');*/
           }
       });
       return false;
    });
})();