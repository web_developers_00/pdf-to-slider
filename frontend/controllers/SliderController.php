<?php

namespace frontend\controllers;

use Yii;
use common\models\Slider;
use common\models\Image;
use common\models\SliderSearch;

use common\helpers\ZipHelper;
use common\helpers\UtilsHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use yii\filters\VerbFilter;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post())) {
            $model->pdfFile = UploadedFile::getInstance($model, 'pdfFile');
            if ($model->save()) {
                $path = Yii::getAlias('@uploads') . '/' . $model->id . '/';
                if (FileHelper::createDirectory($path)) {
                    if ($model->upload($path)) {
                        $document = new \Imagick($path . $model->id . '.pdf');
                        $count = $document->getNumberImages();
                        if ($count <= 50) {
                            if (FileHelper::createDirectory($path . 'images/')) {
                                for ($x = 1; $x <= $document->getNumberImages(); $x++) {
                                    $document->previousImage();
                                    $imageName = 'img'.$count.'.jpeg';
                                    $image = new Image();
                                    $image->name = $imageName;
                                    $image->slider_id = $model->id;
                                    $image->image_link = Url::home(true) . 'uploads/' . $model->id . '/images/' . $imageName;
                                    if ($image->save()) {
                                        $document->writeImage($path . 'images/' . 'img'.$count.'.jpeg');
                                        $count--;
                                    }
                                }

                                //открыть файл, если не существует, то создать его
                                $fp = fopen($path . "index.html", "w");
                                 
                                //записать в файл сгенерированную по шаблону страницу
                                fwrite($fp, $this->renderPartial('_slider_index', ['model' => $model]));
                                 
                                //закрыть файл
                                fclose($fp);

                                UtilsHelper::recurse_copy(Yii::getAlias('@uploads') . '/assets', Yii::getAlias('@uploads') . '/' . $model->id . '/assets');

                                //удалить pdf файл из директории слайдера
                                unlink($path . $model->id . '.pdf');

                                return $this->redirect(['view', 'id' => $model->id]);
                            } else {
                                Yii::$app->session->setFlash('error', 'Не удалось создать директорию для изображений, попробуйте еще раз.');
                                $this->findModel($model->id)->delete();
                                FileHelper::removeDirectory($path);
                                return $this->render('create', [
                                    'model' => $model,
                                ]);
                            }
                        } else {
                            Yii::$app->session->setFlash('error', 'Количество страниц pdf файла не должно превышать 50.');
                            $this->findModel($model->id)->delete();
                            FileHelper::removeDirectory($path);
                            return $this->render('create', [
                                'model' => $model,
                            ]);
                        }
                    } else {
                        Yii::$app->session->setFlash('error', 'Не удалось загрузить файл pdf.');
                        $this->findModel($model->id)->delete();
                        FileHelper::removeDirectory($path);
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось создать директорию слайдера, попробуйте еще раз');
                    $this->findModel($model->id)->delete();
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось сохранить слайдер, попробуйте еще раз');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }                  
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
    *
    * Метод для выгрузки архива со слайдером
    * Сгенерированный архив хранится в папке слайдера 30 минут, по истечении которых удаляется из папки слайдера cron-планировщиком
    *
    */
    public function actionDownloadSlider($id) {
        $model = $this->findModel($id);
        $path = Yii::getAlias('@uploads') . '/' . $model->id . '/' . $model->id . '.zip';
        if (!file_exists($path)) {
            $za = new ZipHelper;
            $res = $za->open($path, \ZipArchive::CREATE);
            $za->addDir(Yii::getAlias('@uploads') . '/' . $model->id, basename(Yii::getAlias('@uploads') . '/' . $model->id));
            $za->close();
        } 

        //установить заголовок ответа и отдать архив
        header("Content-Disposition: attachment; filename=\"".basename($path)."\"");
        header("Content-Length: ".filesize($path));
        readfile($path);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
