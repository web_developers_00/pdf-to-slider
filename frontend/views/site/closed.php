<?php

$this->title = 'Запись на личный приём к Главе администрации';
?>

<div class="row text-center">
    <div class="col-md-12">
        <div class="alert alert-warning block-shadow" role="alert">
            <p>
                Электронная запись на личный прием к Главе администрации закрыта!
            </p>
        </div>
    </div>
</div>
