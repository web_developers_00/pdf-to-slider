<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Главная страница';
?>
<div class="site-index">
	<div class="jumbotron">
		<h1>Конвертер PDF-файла в HTML слайдер</h1>
		<p>...</p>
		<p>
			<?= 
				Html::a('Создать слайдер!', ['/slider/create'], [
                                'title'=>'Создать слайдер!',
                                'class' => 'btn btn-primary btn-lg'
                            ]
                        )
            ?>
		</p>
	</div>
</div>
