<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name'
            ],
            [
                'attribute' => 'create_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel, 
                    'attribute' => 'create_date',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'Выберите дату создания...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]),
                'headerOptions' => ['style' => 'width:30%'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{download} {view} {delete}',
                'buttons' => [
                    'view' => function($url, $model) {
                        return Html::a('Просмотр', ['view?id='.$model->id],
                            [
                                'title'=>'Просмотр',
                                'class' => 'btn btn-primary'
                            ]
                        );
                    },
                    'download' => function($url, $model) {
                        return Html::a('Скачать слайдер', ['download-slider?id=' . $model->id],
                            [
                                'title'=>'Скачать слайдер',
                                'class' => 'btn btn-success'
                            ]
                        );
                    },
                    'delete' => function($url, $model) {
                        return Html::a('Удалить', ['delete?id='.$model->id],
                            [
                                'title'=>'Удалить',
                                'data-confirm' => Yii::t('yii', 'Вы действительно хотите удалить запись?'),
                                'data-method' => 'post',
                                'class' => 'btn btn-danger',
                            ]
                        );
                    },
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
        ],
    ]); ?>
</div>
