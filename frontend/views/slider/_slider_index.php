<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<?php $this->beginContent('@app/views/layouts/main_index.php'); ?>

    <div class="slider-view">

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators" style="background-color: rgba(199, 199, 199, 0.56);bottom: 0;">
                <?php
                    $images = array_reverse($model->images);
                    foreach ($images as $key => $image) {

                        echo $this->render('_carousel_indicators', [
                            'key' => $key
                        ]);
                        
                    }
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                
                <?php
                    $images = array_reverse($model->images);
                    foreach ($images as $key => $image) {

                        echo $this->render('_slider_item_index', [
                            'image' => $image,
                            'key' => $key
                        ]);
                        
                    }
                ?>

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <p>
            <?php //Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php /*Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])*/ ?>
        </p>

        <?php /*DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'pdf_link',
                'create_date',
            ],
        ])*/ ?>

    </div>

<?php $this->endContent(); ?>

