<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/jquery.kladr.min.css',
        /*'css/bootstrap.css',
        'css/bootstrap-social.css',
        'css/bootstrap-theme.min.css',
        'css/font-awesome.css',*/
    ];
    public $js = [
        'js/angular.min.js',
        'js/order.module.js',
		//'js/jquery-1.11.3.min.js',
        'js/buttonNonActive.js',
        'js/anotherOrganization.js',
        'js/jquery.kladr.min.js',
        'js/addresses.module.js',
        //'js/bootstrap.min.js',
        //'js/init.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
