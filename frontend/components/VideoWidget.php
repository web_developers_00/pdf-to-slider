<?php
namespace frontend\components;
 
use yii\base\Widget;
use yii\helpers\Html;

class VideoWidget extends Widget
{
 public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('../../views/site/_video',['model' => $this->model]);
    }
}
?>