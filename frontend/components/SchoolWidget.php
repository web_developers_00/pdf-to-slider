<?php
namespace frontend\components;
 
use yii\base\Widget;
use yii\helpers\Html;

class SchoolWidget extends Widget
{
 public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('../../views/site/_school',['model' => $this->model]);
    }
}
?>