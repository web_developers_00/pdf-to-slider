<?php
namespace frontend\components;
 
use yii\base\Widget;
use yii\helpers\Html;

class PartnerWidget extends Widget
{
 public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('../../views/site/_partner',['model' => $this->model]);
    }
}
?>