<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `image`.
 */
class m170604_151324_create_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('image', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'image_link' => Schema::TYPE_STRING,
            'create_date' => Schema::TYPE_DATE . ' NOT NULL',
            'slider_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ]);
        $this->createIndex('fk_slider_id', 'image', 'slider_id');
        $this->addForeignKey('fk_slider_id', 'image', 'slider_id', 'slider', 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('image');
    }
}
