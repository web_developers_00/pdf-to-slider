<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `slider`.
 */
class m170604_150721_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('slider', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'pdf_link' => Schema::TYPE_STRING,
            'create_date' => Schema::TYPE_DATE . ' NOT NULL',
            'slider_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('slider');
    }
}
