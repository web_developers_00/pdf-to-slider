angular.module('orderApp', []);

//filters
angular.module('orderApp')
	.filter('timeformat', function() {
		return function(input) {
			input = input || '';
			var output = "";
			input = input.split(':');
			output = input[0] + ':' + input[1];
			return output;
		};
	});
angular.module('orderApp')
	.filter('dateformat', function() {
		return function(input) {
			input = input || '';
			var output = '',
				month = {
					'01': 'Января',
					'02': 'Февраля',
					'03': 'Марта',
					'04': 'Апреля',
					'05': 'Мая',
					'06': 'Июня',
					'07': 'Июля',
					'08': 'Августа',
					'09': 'Сентября',
					'10': 'Октября',
					'11': 'Ноября',
					'12': 'Декабря',
				};
			input = input.split('-');
			output = input[2] + ' ' + month[input[1]] + ' ' + input[0] + ' года';

			return output;
		};
	});

//factories
angular.module('orderApp').factory('ShareFactory', function () {
    var mem = {};
 
    return {
        store: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});

//controllers
angular.module('orderApp').controller('ErrorController', function($scope, ShareFactory) {

	ShareFactory.store('errorControllerScope', $scope);

	$scope.alerts = {};

	$scope.addAlert = function(message, type) {
		$scope.alerts[type] = $scope.alerts[type]||[];
		$scope.alerts[type].push(message);
	};

	$scope.clearAlerts = function() {
		for(var x in $scope.alerts) {
           delete $scope.alerts[x];
        }
	};
});

angular.module('orderApp').controller('DayController', function($scope, $http, $interval, $window, ShareFactory) {
	$scope.receptionDays = [];
	$scope.receptionTimeCounter = 0;
	$scope.errorControllerScope = ShareFactory.get('errorControllerScope');

	$scope.getReceptionDays = function() {
		$http.post('http://queue-record/site/get-reception-days', {
			_csrf: yii.getCsrfToken(),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			}
		}).success(function(receptionDays) {
			$scope.errorControllerScope.clearAlerts();
			if (receptionDays.length) {
				if (!$scope.receptionDays.length) {
					//first init times
					$scope.receptionDays = receptionDays;
				} else {
					$scope.refreshReceptionDays(receptionDays, $scope.receptionDays);
				}
				/*$scope.receptionTimeCounter = $scope.getReservedTimes($scope.receptionDay.receptionTimes);
				if ($scope.receptionTimeCounter == $scope.receptionDay.receptionTimes.length) $window.location.reload();*/
			} else {
				//$window.location.reload();
				$scope.receptionDays = [];
			}
		}).error(function(error) {
			$scope.errorControllerScope.addAlert('Не удалось загрузить дни приёма', 'alert-danger');
			$scope.receptionDays = {};
		}).finally(function() {
		});
	};
	$scope.getReceptionDay = function() {
		$http.post('http://queue-record/site/get-nearest-reception-day', {
			_csrf: yii.getCsrfToken(),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
		}).success(function(receptionDay) {
			$scope.errorControllerScope.clearAlerts();
			if (receptionDay && receptionDay.receptionTimes.length) {
				if (!$scope.receptionDay.receptionTimes || ($scope.receptionDay.id != receptionDay.id)) {
					//first init times
					$scope.receptionDay = receptionDay;
				} else {
					$scope.refreshReceptionTimes(receptionDay.receptionTimes, $scope.receptionDay.receptionTimes);
				}
				$scope.receptionTimeCounter = $scope.getReservedTimes($scope.receptionDay.receptionTimes);
				if ($scope.receptionTimeCounter == $scope.receptionDay.receptionTimes.length) $window.location.reload();
			} else {
				$window.location.reload();
			}
		}).error(function(error) {
			$scope.errorControllerScope.addAlert('Не удалось загрузить время приёма', 'alert-danger');
			$scope.receptionDay = {};
		}).finally(function() {
		});
	};
	$scope.getReceptionDayTimes = function() {
		$http.post('http://queue-record/site/get-reception-day-times', {
			_csrf: yii.getCsrfToken(),
			data: {
				receptionDayId: ($scope.receptionDay) ? ($scope.receptionDay.id) : (''),
			},
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
		}).success(function(receptionTimes) {
			$scope.errorControllerScope.clearAlerts();
			if (receptionTimes) {
				$scope.refreshReceptionTimes(receptionTimes, $scope.receptionDay.receptionTimes);
				$scope.receptionTimeCounter = $scope.getReservedTimes($scope.receptionDay.receptionTimes);
				//if ($scope.receptionTimeCounter == $scope.receptionDay.receptionTimes.length) $window.location.reload();
			}/* else {
				$window.location.reload();
			}*/
		}).error(function(error) {
			/*$scope.errorControllerScope.addAlert('Не удалось загрузить время приёма', 'alert-danger');
			$scope.receptionDay = {};*/
		}).finally(function() {
		});
	};
	$scope.getReservedTimes = function(receptionTimes) {
		var counter = 0;
		for (var i = 0, maxi = receptionTimes.length; i < maxi; i++) {
			if (receptionTimes[i].status == 1) counter++;
		}
		console.log(counter + " " + $scope.receptionDay.receptionTimes.length);
		return counter;
	};

	$scope.refreshReceptionDays = function(freshReceptionDays, currentReceptionDays) {
		for (var i = 0, maxi = currentReceptionDays.length; i < maxi; i++) {
			$scope.removeWastedReceptionDay(i, currentReceptionDays[i], freshReceptionDays, currentReceptionDays);
		}
		for (var i = 0, maxi = freshReceptionDays.length; i < maxi; i++) {
			$scope.addNewReceptionDay(i, freshReceptionDays[i], currentReceptionDays, freshReceptionDays);
		}
	};
	$scope.removeWastedReceptionDay = function(index, currentReceptionDay, freshReceptionDays, currentReceptionDays) {
		var founded = false;
		for (var i = 0, maxi = freshReceptionDays.length; i < maxi; i++) {
			if (freshReceptionDays[i].id == currentReceptionDay.id) {
				founded = true;
				currentReceptionDay.status = freshReceptionDays[i].status;
			}
		}
		if (!founded) {
			currentReceptionDays.splice(index, 1);
		}
	};
	$scope.addNewReceptionDay = function(index, freshReceptionDay, currentReceptionDays, freshReceptionDays) {
		for (var i = 0, maxi = currentReceptionDays.length; i < maxi; i++) {
			if (currentReceptionDays[i].id == freshReceptionDay.id) {
				return false;
			}
		}
		currentReceptionDays.push(freshReceptionDay);
	};

	$scope.refreshReceptionTimes = function(freshReceptionTimes, currentReceptionTimes) {
		for (var i = 0, maxi = currentReceptionTimes.length; i < maxi; i++) {
			$scope.removeWastedReceptionTime(i, currentReceptionTimes[i], freshReceptionTimes, currentReceptionTimes);
		}
		for (var i = 0, maxi = freshReceptionTimes.length; i < maxi; i++) {
			$scope.addNewReceptionTime(i, freshReceptionTimes[i], currentReceptionTimes, freshReceptionTimes);
		}
	};
	$scope.removeWastedReceptionTime = function(index, currentReceptionTime, freshReceptionTimes, currentReceptionTimes) {
		var founded = false;
		for (var i = 0, maxi = freshReceptionTimes.length; i < maxi; i++) {
			if (freshReceptionTimes[i].id == currentReceptionTime.id) {
				founded = true;
				currentReceptionTime.status = freshReceptionTimes[i].status;
			}
		}
		if (!founded) {
			currentReceptionTimes.splice(index, 1);
		}
	};
	$scope.addNewReceptionTime = function(index, freshReceptionTime, currentReceptionTimes, freshReceptionTimes) {
		for (var i = 0, maxi = currentReceptionTimes.length; i < maxi; i++) {
			if (currentReceptionTimes[i].id == freshReceptionTime.id) {
				return false;
			}
		}
		currentReceptionTimes.push(freshReceptionTime);
	};

	//init model
	angular.element(document).ready(function () {
		/*$scope.getReceptionDay();
		$interval($scope.getReceptionDay, 3000);*/
		$scope.getReceptionDays();
		$interval($scope.getReceptionDays, 3000);
		//$interval($scope.getReceptionDayTimes, 3000);
		$scope.$watch('receptionDay', function(newValue, oldValue) {
			if ( newValue ) {
				if ( angular.isDefined($scope.receptionTimesUpdater) ) return;
				$scope.receptionTimesUpdater = $interval($scope.getReceptionDayTimes, 3000);
			} else {
				$interval.cancel($scope.receptionTimesUpdater);
            	$scope.receptionTimesUpdater = undefined;
			}
		});
		//$interval($scope.getReceptionDay, 3000);
	});
});

//bootstraping module
//angular.bootstrap(document.getElementById("studentOrderApp"), ['studentOrderApp']);