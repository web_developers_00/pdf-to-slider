<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use common\models\Slider;

/**
*
* Rest-контроллер
*
*/
class SliderController extends ActiveController {

	//модель контроллера
	public $modelClass = 'common\models\Slider';

	/**
	*
	* Rest api метод для получения информации по слайдеру и ссылок на его изображения
	*
	*/
	public function actionGetSlider($id) {
		$user = Slider::find()
			->with('images')
			->where('id = :id', [':id' => $id])
			->asArray()
			->one();
		return $user;
	}

}