<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_link
 * @property string $create_date
 * @property integer $slider_id
 *
 * @property Slider $slider
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    public function behaviors() { 
        return [ 
            'timestamp' => [ 
                'class' => 'yii\behaviors\TimestampBehavior', 
                'attributes' => [ 
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ], 
                'value' => function() { 
                    return date('Y-m-d');
                }
            ]
        ]; 
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id'], 'required'],
            [['create_date'], 'safe'],
            [['slider_id'], 'integer'],
            [['name', 'image_link'], 'string', 'max' => 255],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slider::className(), 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image_link' => 'Ссылка на изображение',
            'create_date' => 'Дата создания',
            'slider_id' => 'Слайдер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
    }
}
