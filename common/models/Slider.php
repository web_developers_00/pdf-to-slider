<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $name
 * @property string $pdf_link
 * @property string $create_date
 *
 * @property Image[] $images
 */
class Slider extends \yii\db\ActiveRecord
{

    /**
     * @var UploadedFile
     */
    public $pdfFile;

    public function behaviors() { 
        return [ 
            'timestamp' => [ 
                'class' => 'yii\behaviors\TimestampBehavior', 
                'attributes' => [ 
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ], 
                'value' => function() { 
                    return date('Y-m-d');
                }
            ]
        ]; 
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['create_date', 'name', 'pdf_link'], 'safe'],
            [['name', 'pdf_link'], 'string', 'max' => 255],
            [['pdfFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf', 'maxSize' => 1024 * 1024 * 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'pdf_link' => 'Ссылка',
            'create_date' => 'Дата создания',
            'pdfFile' => 'Файл PDF',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['slider_id' => 'id']);
    }

    public function upload($path) {
        if ($this->validate()) {
            //$fileName = iconv('utf-8', 'cp1251', $this->name);
            $this->pdfFile->saveAs($path . $this->id . '.' . $this->pdfFile->extension);
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {

            Image::deleteAll('slider_id = :id', [
                'id' => $this->id
            ]);

            FileHelper::removeDirectory(Yii::getAlias('@uploads') . '/' . $this->id . '/');

            return true;

        } else {
            return false;
        }
    }
}
