<?php

namespace common\helpers;

class UtilsHelper {

  public static function recurse_copy($src, $dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
      if (( $file != '.' ) && ( $file != '..' )) { 
        if ( is_dir($src . '/' . $file) ) { 
          UtilsHelper::recurse_copy($src . '/' . $file, $dst . '/' . $file); 
        } else { 
          copy($src . '/' . $file, $dst . '/' . $file); 
        } 
      } 
    } 
    closedir($dir); 
  }

}
