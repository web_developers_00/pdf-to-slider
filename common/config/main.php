<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    /*'timeZone' => 'UTC',*/
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'aliases' => [
        //@uploads => 'uploads/',
        @schedulePath => 'uploads',
        @queueRecord => 'http://queue-record',
    ],
];
