<?php
return [
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=pdf_to_slider',
            'username' => 'root',
            'password' => '12345678',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                'class' => 'Swift_MailTransport',
			],	
			'useFileTransport' => false,
		],	
        'encrypter' => [
            'class' => '\nickcv\encrypter\components\Encrypter',
            'globalPassword' => '5thElement',
            'iv' => 'qnmjghtyucfkmnvf',
            'useBase64Encoding' => true,
            'use256BitesEncoding' => false,
        ],
	],
	'language'=>'ru',
];
